import { Component } from '@angular/core';
import { AuthService } from './authenticate/auth.service';
import { HttpClientModule } from '@angular/common/http';
import { Router } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { UserService } from './services/user/user.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'app';

  constructor(private fb: FormBuilder,
    private myRoute: Router,
    private auth: AuthService, private http: HttpClientModule, private clientService: UserService) {
    }
}
