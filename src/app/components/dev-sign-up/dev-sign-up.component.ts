import { AuthService } from './../../authenticate/auth.service';
import { SignUpRequest } from './../../services/user/sign-up-request';
import { Router } from '@angular/router';
import { UserService } from './../../services/user/user.service';
import { UserSkill } from './../../services/user-skill/user-skill';
import { User } from './../../services/user/user';
import { Component, OnInit } from '@angular/core';
import { StoreService } from './../../services/_store/store.service';

@Component({
  selector: 'app-dev-sign-up',
  templateUrl: './dev-sign-up.component.html',
  styleUrls: ['./dev-sign-up.component.scss']
})
export class DevSignUpComponent implements OnInit {

  inputFullname: string;
  user: User;
  userSkills: UserSkill[];

  constructor(private store: StoreService, private userService: UserService, private router: Router, private authService: AuthService) {
    this.user = new User;
    this.userSkills = [] ;
   }

  ngOnInit() {
    console.log('BONJOUR');
    console.log(this.store.inductionSkills);
    if (this.store.inductionSkills) { this.userSkills = this.store.transitUserSkills; console.log(this.userSkills); }
  }

  onSubmit() {
    let signUpRequest: SignUpRequest;
    signUpRequest = new SignUpRequest();
    signUpRequest.username = this.user.login;
    signUpRequest.ville = this.user.city;
    signUpRequest.email = this.user.mail;
    signUpRequest.password = this.user.password;
    signUpRequest.lookingForWork = !!this.user.lookingForWork;
    signUpRequest.totalScore = this.store.inductionSkills
    .map(element => element.level)
    .reduce((memo, val) => memo + val);

    this.userService.subscribe(signUpRequest).subscribe(result => {
      let dev;
      dev = result;

      for (const entry of this.userSkills) {
        entry.id.developper = dev ;
        console.log(entry.id.developper);
      }

      this.userService.addUserSkills(this.userSkills).subscribe(() => {
        console.log(this.userSkills);

        const getDev = this.userSkills[0].id.developper;
        this.store.connectedDev = {
          id: getDev.id,
          login: getDev.login.toString(),
          totalScore: getDev.totalScore
        };
        this.authService.loginSpring(this.user.login, this.user.password).subscribe(data => {
          this.router.navigate(['/devmain']);
        }, err => {
          console.log(err);
          //this.loading = false;
        });

   });




    });

  }

}
