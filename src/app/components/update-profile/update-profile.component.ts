import { HttpClient } from '@angular/common/http';
import { UserService } from './../../services/user/user.service';
import { User } from './../../services/user/user';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-update-profile',
  templateUrl: './update-profile.component.html',
  styleUrls: ['./update-profile.component.scss']
})
export class UpdateProfileComponent implements OnInit {

  private id: number;
  user: User;
  private err = false;


  constructor (private route: ActivatedRoute, private router: Router, private userService: UserService) {
    this.user = new User();
  }
  onSubmit() {
    console.log(this.user);
    if (this.user.roles[0].name === 'ROLE_DEV') {
      this.userService.saveDev(this.user).subscribe(res => this.gotoUserList());
    } else {
      this.userService.saveCompany(this.user).subscribe(res => this.gotoUserList());
    }
    let newUser = JSON.parse(localStorage.getItem('currentUser'));
    newUser.user = this.user;
    localStorage.setItem('currentUser', JSON.stringify(newUser));
  }

  gotoUserList() {
// tslint:disable-next-line: no-unused-expression
    this.router.navigate[('/updateprofile')];
  }

  ngOnInit() {
    this.user = JSON.parse(localStorage.getItem('currentUser')).user;
    console.log('INFOOOOOOOOO');
      console.log(JSON.parse(localStorage.getItem('currentUser')).user);
  }

}
