import { Component, OnInit } from '@angular/core';
import { faUser } from '@fortawesome/free-solid-svg-icons';
import { UserService } from './../../services/user/user.service';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from './../../authenticate/auth.service';
import { HttpClientModule } from '@angular/common/http';


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  faUser = faUser;
  menuClosed = false;

  constructor(private fb: FormBuilder,
    private myRoute: Router,
    private auth: AuthService, private http: HttpClientModule, private clientService: UserService) { }

  ngOnInit() {
  }

  toggleMenu() {
    this.menuClosed = !this.menuClosed;
  }

}
