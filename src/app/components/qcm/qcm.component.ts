import { UserService } from './../../services/user/user.service';
import { ResultQcm } from './../../services/result-qcm/result-qcm';
import { Answer } from './../../services/answer/answer';
import { QcmService } from './../../services/qcm/qcm.service';
import { Qcm } from './../../services/qcm/qcm';
import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Question } from '../../services/question-answer/question';
import { templateJitUrl } from '@angular/compiler';


@Component({
  selector: 'app-qcm',
  templateUrl: './qcm.component.html',
  styleUrls: ['./qcm.component.scss']
})
export class QcmComponent implements OnInit {
  qcm: Qcm;
  title = 'Qcm Example';
  radioData: any;
  constructor (private httpService: HttpClient, private qcmService: QcmService, private userService: UserService) {
  }

  ngOnInit () {
    this.qcmService.findById(9).subscribe(data => {
      this.qcm = data;
    });
  }

  calculPoints(): number {
    let tmp = 0;

  this.qcm.questions.forEach((element) => {
    element.answers.forEach((element2) => {
      //console.log(element2);
      if (element2.trueOrFalse && this.radioData === element2.number) {
        tmp += 3;
      }
    });
  });

    return tmp;
  }

  onSubmit() {
   // console.log(this.calculPoints());
    console.log('User id: ' + localStorage.getItem('UserId'));
    console.log('Score: ' + this.calculPoints());

    /* this.resultQcm.score = this.calculPoints();
    this.resultQcm.qcm = this.qcm;
    this.resultQcm.id = null;*/
    this.userService.addResultQcm(this.qcm, +localStorage.getItem('UserId'), this.calculPoints()).subscribe(result =>
    this.gotoResultPage());
  }
  gotoResultPage() {
    //throw new Error("Method not implemented.");
  }

}
