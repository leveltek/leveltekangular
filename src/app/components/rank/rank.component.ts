import { UserSkill } from './../../services/user-skill/user-skill';
import { Component, OnInit } from '@angular/core';
import { faUser } from '@fortawesome/free-solid-svg-icons';
import { StoreService } from '../../services/_store/store.service';
import { UserService } from '../../services/user/user.service';

@Component({
  selector: 'app-rank',
  templateUrl: './rank.component.html',
  styleUrls: ['./rank.component.scss']
})
export class RankComponent implements OnInit {

  faUser = faUser;

  alldevs = [];
  devs = [];

  lookingForWork: boolean = false;

  constructor(private userService: UserService, private store: StoreService) { }

  ngOnInit() {
    const observable = this.userService.devsFindAll();
    observable.subscribe((devs: any) => {

      // la fonction suivante 'sort' permet de trier le tableau d'objet par
      // ordre décroissant du totalScore de l'utilisateur
      devs.sort(function (a, b) {
        return b.totalScore - a.totalScore;
      });

      // ici on va récupérer tous les devs
      // et on va ajouter un tooltip à chaque dev avec les compétences et les niveaux
      this.alldevs = devs.map((dev) => {
        let tooltip = '';

        // tslint:disable-next-line: no-unused-expression
        dev && dev.userSkills && dev.userSkills.forEach(element => {
          tooltip = `${tooltip} ${element.id.skill.name} (${element.score})`;
        });

        dev.tooltip = tooltip;
        return dev;
      });
      this.devs = this.alldevs;
    });
  }
  toggleLookingForWork() {
    this.devs = this.lookingForWork ? this.alldevs.filter(dev => dev.lookingForWork === true) : this.alldevs;
  }
}
