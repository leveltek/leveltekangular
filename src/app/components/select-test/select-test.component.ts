import { Component, OnInit } from '@angular/core';
import { StoreService } from './../../services/_store/store.service';
import { Skill } from './../../services/skill/skill';
import { InductionSkill } from '../add-skills/add-skills.component';

@Component({
  selector: 'app-select-test',
  templateUrl: './select-test.component.html',
  styleUrls: ['./select-test.component.scss']
})
export class SelectTestComponent implements OnInit {
  inducSkills;
  name: any;



  constructor( private store: StoreService) { }

  ngOnInit() {
    console.log(this.store.inductionSkills);
    this.inducSkills = this.store.inductionSkills;
  }

  onSelectionne(event, skillName) {
    console.log('skillName_', skillName);

    this.store.selectSkill = skillName;

/*
    const inductionSkill: InductionSkill = {

      name: event.target.textContent.trim(),

    };
    // Fusionne deux tableaux on les collant bout a bout
    this.slectSkill = [...this.inductionSkills, inductionSkill];
  */
  }
}
