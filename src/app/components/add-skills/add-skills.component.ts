import { UserSkill } from './../../services/user-skill/user-skill';
import { Component, OnInit, SimpleChanges, OnDestroy } from '@angular/core';
import { SkillApiService } from '../../services/skill/skill-api.service';
import { StoreService } from '../../services/_store/store.service';

// Création et utilisation de l'objet InductionSkills lorsque l'utilisateur n'est pas connecté
export interface InductionSkill {
  id: number;
  name: string;
  level: number;
}

@Component({
  selector: 'app-add-skills',
  templateUrl: './add-skills.component.html',
  styleUrls: ['./add-skills.component.scss']
})
export class AddSkillsComponent implements OnInit, OnDestroy {

  inputSkill: String = '';

  results = [];
  canAddSkill = false;

  inductionSkills: InductionSkill[] = [];

  constructor(private api: SkillApiService, private store: StoreService) { }

  ngOnInit() {
    if (this.store.inductionSkills) { this.inductionSkills = this.store.inductionSkills; }
  }

  onChange(newValue: string) {
    const observable = this.api.getSkillsByText(newValue);
    observable.subscribe((r: any) => {
      this.canAddSkill = true;

      // après avoir récupérer la liste des compétences recherchées
      // .filter filtre le tableau d'elements sur une condition

      this.results = r.filter((element) => {
        if (element.name.toLocaleLowerCase() === this.inputSkill.toLocaleLowerCase()) {
          this.canAddSkill = false;
        }
        const skillIds = this.inductionSkills.map(mySkill => mySkill.id);
        return skillIds.indexOf(element.id) === -1;
      });

      if (this.inputSkill === '') { this.results = []; }
    });
  }

  onSelect(event, id) {
    this.results = [];
    this.inputSkill = '';

    const inductionSkill: InductionSkill = {
      id,
      name: event.target.textContent.trim(),
      level: 1
    };
    // Fusionne deux tableaux on les collant bout a bout
    this.inductionSkills = [...this.inductionSkills, inductionSkill];
  }
  onRemoveSkill(event, id) {
    this.inductionSkills = this.inductionSkills.filter( element => element.id !== id);
  }
  addSkill() {
    const inputSkill = this.inputSkill.toString();
    this.api.addSkill(inputSkill).subscribe((result: any) => {

    const inductionSkill: InductionSkill = {
      id: result.id,
      name: result.name,
      level: 1
    };
    this.inductionSkills = [...this.inductionSkills, inductionSkill];
    this.canAddSkill = false;
    this.inputSkill = '';
  });
}
  ngOnDestroy() {
    this.store.inductionSkills = this.inductionSkills;
 }

  onSubmit() {

  }
}
