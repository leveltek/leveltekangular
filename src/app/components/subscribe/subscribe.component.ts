import { SignUpRequest } from './../../services/user/sign-up-request';
import { Component, OnInit } from '@angular/core';
import { User } from '../../services/user/user';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from '../../services/user/user.service';
import { empty } from 'rxjs';

@Component({
  selector: 'app-subscribe',
  templateUrl: './subscribe.component.html',
  styleUrls: ['./subscribe.component.scss']
})
export class SubscribeComponent implements OnInit {

  client: SignUpRequest;

  constructor(private route: ActivatedRoute, private router: Router, private clientService: UserService) {
    this.client = new SignUpRequest();
  }

  onSubmit() {
    console.log(this.client);
    /*
    if(!this.client.name){
      this.clientService.saveDev(this.client).subscribe(result => this.gotoUserList());
    } else {
      this.clientService.saveCompany(this.client).subscribe(result => this.gotoUserList());
    }*/
    this.clientService.subscribe(this.client).subscribe(result => this.gotoUserList());

  }

  gotoUserList() {
    this.router.navigate(['/home']);
  }

  ngOnInit() {
    console.log("BONJOUR");
  }

}
