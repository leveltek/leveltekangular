import { Component, OnInit } from '@angular/core';
import { StoreService } from './../../services/_store/store.service';

@Component({
  selector: 'app-test-dev',
  templateUrl: './test-dev.component.html',
  styleUrls: ['./test-dev.component.scss']
})
export class TestDevComponent implements OnInit {
  skill;


  constructor(private store: StoreService) { }

  ngOnInit() {
    this.skill = this.store.selectSkill;
  }

}
