import { UserService } from './../../services/user/user.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { StoreService } from '../../services/_store/store.service';
import { UserSkill } from '../../services/user-skill/user-skill';
import { User } from '../../services/user/user';
import { UserSkillKey } from '../../services/user-skill-key/user-skill-key';
import { Skill } from '../../services/skill/skill';

@Component({
  selector: 'app-level',
  templateUrl: './level.component.html',
  styleUrls: ['./level.component.scss']
})
export class LevelComponent implements OnInit {

  inductionSkills = [];

  niveaux = [
    'Débutant : Je me suis documenté, j\'ai lu un cours, un blog ...',
    'Débutant : J\'ai fait quelques exercices, un tutoriel pratique',
    'Débutant : J\'ai fait un projet perso',
    'Intermédiaire : J\'ai fait un projet pro ou 2 projets perso',
    'Intermédiaire : J\'ai fait 2 projets pro ou 3 projets perso',
    'Intermédiaire : J\'ai fait 3 projets pro',
    'Senior : J\'ai fait plus de 3 projets pro',
    'Senior : J\'ai donné quelques cours',
    'Senior : Je n\'arrive plus à compter le nombre d\'années',
    'Expert : Vous êtes incollable et prêt à relever n\'importe quel défi !'
  ];

  score = 0;

  constructor(private store: StoreService, private router: Router, private userService: UserService) { }

  computeScore() {
    let score = 0;
    this.inductionSkills.forEach(item => score = score + item.level);
    this.score = score;
    this.store.inductionSkills = this.inductionSkills;
  }

  ngOnInit() {
    if (this.store.inductionSkills) {
      this.inductionSkills = this.store.inductionSkills;
      this.computeScore();
    } else {
      this.router.navigate(['/addskills']);
    }
  }
  handleChange(e, id) {

    this.inductionSkills = this.inductionSkills.map(element => {
      if (element.id === id) {
        element.level = e.value;
      }
      return element;
    });
    this.computeScore();
  }
  onNext() {
    this.router.navigate(['/devsignup']);


    //Test insert by UserSkill[]

    let userSkills: UserSkill[];
    userSkills = [];
    let tmpUserSkill: UserSkill;
    let tmpUserSkillKey: UserSkillKey;
    let tmpSkill: Skill;

      console.log(this.inductionSkills);
    for (const entry of this.inductionSkills) {
      tmpUserSkill = new UserSkill();
      tmpUserSkillKey = new UserSkillKey();
      tmpSkill = new Skill();


      tmpSkill.id = entry.id;
      tmpSkill.name = entry.name;
      //tmpUserSkillKey.developper = dev;
      tmpUserSkillKey.skill = tmpSkill;
      tmpUserSkill.id = tmpUserSkillKey;
      tmpUserSkill.score = entry.level;

      userSkills.push(tmpUserSkill);

      /*
      this.userService.addUserSkills(userSkills).subscribe(result => {
        console.log(JSON.stringify(result));
        console.log('Success');

      });*/

     }
     this.store.transitUserSkills = userSkills;
     // Fin insert

  }
  onBack() {
    this.router.navigate(['/addskills']);
  }
}
