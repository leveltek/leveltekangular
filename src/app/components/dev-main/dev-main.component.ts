import { Component, OnInit } from '@angular/core';
import { StoreService } from './../../services/_store/store.service';
import { User } from '../../services/user/user';
import { SkillService } from '../../services/skill/skill.service';
import { Skill } from '../../services/skill/skill';
import { InductionSkill } from '../add-skills/add-skills.component';
import { UserSkill } from '../../services/user-skill/user-skill';
import { UserService } from '../../services/user/user.service';
import { Router } from '@angular/router';
import { UserSkillKey } from '../../services/user-skill-key/user-skill-key';

interface Data {
  item_id: number;
  item_text: string;
}

@Component({
  selector: 'app-dev-main',
  templateUrl: './dev-main.component.html',
  styleUrls: ['./dev-main.component.scss']
})
export class DevMainComponent implements OnInit {

  //totalScore = null;
  user: User;
  isUpdatingSkill = false;
  dropdownList: Data[];
  dropdownSettings = {};

  allSkills: Skill[] ;
  selectedSkills: any[];
  niveaux: any[];
  mapSkills: any;

  //Userskills
  userSkills: UserSkill[];
  userSkillsToDelete: UserSkill[];

  score = 0;

  constructor(private userService: UserService, private router: Router, private store: StoreService, private skillService: SkillService) {
    this.niveaux = [
      'Débutant : Je me suis documenté, j\'ai lu un cours, un blog ...',
      'Débutant : J\'ai fait quelques exercices, un tutoriel pratique',
      'Débutant : J\'ai fait un projet perso',
      'Intermédiaire : J\'ai fait un projet pro ou 2 projets perso',
      'Intermédiaire : J\'ai fait 2 projets pro ou 3 projets perso',
      'Intermédiaire : J\'ai fait 3 projets pro',
      'Senior : J\'ai fait plus de 3 projets pro',
      'Senior : J\'ai donné quelques cours',
      'Senior : Je n\'arrive plus à compter le nombre d\'années',
      'Expert : Vous êtes incollable et prêt à relever n\'importe quel défi !'
    ];
   }

  ngOnInit() {
    //this.totalScore = this.store.connectedDev ? this.store.connectedDev.totalScore : null;
    this.user = JSON.parse(localStorage.getItem('currentUser')).user;
    console.log(this.user);

    this.userSkills = [];
    this.selectedSkills = [];
    this.userSkillsToDelete = [];
    this.mapSkills = new Map();

    this.dropdownSettings = {
      singleSelection: false,
      idField: 'item_id',
      textField: 'item_text',
      unSelectAllText: 'UnSelect All',
      allowSearchFilter: true,
      enableCheckAll: false
    };

    const user2 = JSON.parse(JSON.stringify(this.user));

    //this.selectedSkills.push()
    for (const userSkill of this.user.userSkills) {
      //this.selectedSkills.push()
      //userSkill.id.developper = this.user;
      userSkill.id.developper = user2;
      //userSkill.id.developper.userSkills = null;
      const inductionSkill: InductionSkill = {
        id: userSkill.id.skill.id,
        name: String(userSkill.id.skill.name),
        level: userSkill.score
      };

      this.selectedSkills.push({item_id: inductionSkill.id, item_text: inductionSkill.name});
      this.mapSkills.set(inductionSkill.id, inductionSkill);
    }

    const observable = this.skillService.findAll();
      observable.subscribe( data => {
         this.allSkills = data ;
         this.dropdownList = [] ;
         for (const elem of this.allSkills ) {
            let name: string;
            name = String(elem.name);
           this.dropdownList.push({item_id: elem.id, item_text: name});

         }
      });
  }

  updatingSkill() {
    if (this.isUpdatingSkill === true) {
      this.isUpdatingSkill = false;
    } else {
      this.isUpdatingSkill = true;
    }
  }

  onSelect(item: any) {
    const inductionSkill: InductionSkill = {
      id: item.item_id,
      name: item.item_text,
      level: 1
    };
    this.mapSkills.set(item.item_id, inductionSkill);
  }

  onDeSelect(item: any) {
    this.mapSkills.delete(item.item_id);
    let i = 0;
    for (const userSkill of this.user.userSkills) {
      console.log('Loop ' + i);
      console.log('Left: ' + userSkill.id.skill.id);
      console.log('Right: ' + item.item_id);
      if (userSkill.id.skill.id === item.item_id) {
        console.log('Tab userskills avant splice:');
        console.log(this.user.userSkills);
        this.user.userSkills.splice(i, 1);
        console.log('Tab userskills apres splice:');
        console.log(this.user.userSkills);

        //TEST DELETE
        this.userSkillsToDelete.push(userSkill);
      }
      i++;
    }
  }

  handleChange(e, id) {
    const inductionSkill: InductionSkill = this.mapSkills.get(id);
    inductionSkill.level = e.value;
    this.mapSkills.set(id, inductionSkill);

    //console.log(this.mapSkills);
  }

  saveChangesSkill() {
    const copyMap = this.mapSkills;

    console.log('TESTATOS');
    console.log(this.user.userSkills);
    console.log('MAP');
    console.log(copyMap);
    for (const entry of copyMap.entries()) {
      for (const userSkill of this.user.userSkills) {
        //Update d'un userskill existant
        if (entry[0] === userSkill.id.skill.id) {
          userSkill.score = entry[1].level;
          console.log(entry[1].level);
          copyMap.delete(entry[0]);
          //Creation d'unne nouvelle liste d"user skill
/*
          let tmpUserSkill = new UserSkill();
          let tmpUserSkillKey = new UserSkillKey();
          let tmpSkill = new Skill();
          let tmpDev = new User();


          tmpSkill.id = entry[1].id;
          tmpSkill.name = entry[1].name;
          tmpDev = this.user;
          //tmpDev.userSkills = null;
          tmpUserSkillKey.developper = tmpDev;
          tmpUserSkillKey.skill = tmpSkill;
          tmpUserSkill.id = tmpUserSkillKey;
          tmpUserSkill.score = entry[1].level;

          this.userSkills.push(tmpUserSkill);*/
        }
      }
    }
    //On cree les userskill non existants
    console.log('CHECK USERSKILLS 1');
    console.log(this.user.userSkills);

    for (const entry of copyMap.entries()) {
      console.log('PLANTE');
      console.log(this.user.userSkills[0]);
      const userSkill = JSON.parse(JSON.stringify(this.user.userSkills[0]));
      userSkill.id.skill.id = entry[1].id;
      userSkill.id.skill.name = entry[1].name;
      userSkill.score = entry[1].level;
      this.user.userSkills.push(userSkill);

    }
    console.log('CHECK USERSKILLS 2');
    console.log(this.user.userSkills);
    console.log('CHECK USER');
    console.log(this.user);
    //console.log('Userskills');

    // MERDE ICI

    //this.userService.addUserSkills(this.user.userSkills).subscribe(() => {
      /*
    this.userService.addUserSkills(this.userSkills).subscribe(() => {
      console.log('passage dans la méthode');
      this.userService.findById(this.user.id).subscribe(result => {
        console.log('C LA FETE');
        console.log(result);
        localStorage.setItem('currentUser', JSON.stringify(result));
        this.router.navigate(['/devmain']);
      }, err => {
        console.log('mon error' + err);
      });
    });*/

    //Update du score
    let cmpt = 0;
    for(const userSkill of this.user.userSkills){
      cmpt += userSkill.score;
    }
    this.user.totalScore = cmpt;

    //Ajout modif comp
    this.userService.deleteUserSkills(this.userSkillsToDelete).subscribe(() => {
      //Supress TEST
      this.userService.saveDev(this.user).subscribe((result) => {
        const currentUser = JSON.parse(localStorage.getItem('currentUser'));
        currentUser.user = result;
        localStorage.setItem('currentUser', JSON.stringify(currentUser));

        this.ngOnInit();
      });
    });
  }
}
