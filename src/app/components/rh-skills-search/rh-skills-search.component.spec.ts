import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RhSkillsSearchComponent } from './rh-skills-search.component';

describe('RhSkillsSearchComponent', () => {
  let component: RhSkillsSearchComponent;
  let fixture: ComponentFixture<RhSkillsSearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RhSkillsSearchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RhSkillsSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
