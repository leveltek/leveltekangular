import { UserService } from './../../services/user/user.service';
import { InductionSkill } from './../add-skills/add-skills.component';
import { SkillService } from './../../services/skill/skill.service';
import { UserSkill } from './../../services/user-skill/user-skill';
import { Component, OnInit, SimpleChanges, OnDestroy } from '@angular/core';
import { SkillApiService } from '../../services/skill/skill-api.service';
import { StoreService } from '../../services/_store/store.service';
import { Router } from '@angular/router';
import { Skill } from '../../services/skill/skill';
import { SelectItem } from 'primeng/components/common/selectitem';
import { User } from '../../services/user/user';

interface Data {
  item_id: number;
  item_text: string;
}

@Component({
  selector: 'app-rh-skills-search',
  templateUrl: './rh-skills-search.component.html',
  styleUrls: ['./rh-skills-search.component.scss']
})



export class RhSkillsSearchComponent implements OnInit {
  display = false;
  comp: User;

  niveaux: any[];

  allSkills: Skill[] ;
  selectedSkills: any[];

  dropdownList: Data[];
  dropdownSettings = {};

  //Map Reliant dropdown et skills avec id skill en clé et induc skill en valeur
  mapSkills: any;
  score = 0;
  usersSearch: User[];
  counter = 0;
  //Map reliant un id user à une dif de score
  mapDif: any;
  //Tableau contenant les id des profils à afficher
  usersToDisplay: any[];

  constructor(private api: SkillApiService, private store: StoreService, private skillService: SkillService, private userService: UserService) {
    this.niveaux = [
      'Débutant : Je me suis documenté, j\'ai lu un cours, un blog ...',
      'Débutant : J\'ai fait quelques exercices, un tutoriel pratique',
      'Débutant : J\'ai fait un projet perso',
      'Intermédiaire : J\'ai fait un projet pro ou 2 projets perso',
      'Intermédiaire : J\'ai fait 2 projets pro ou 3 projets perso',
      'Intermédiaire : J\'ai fait 3 projets pro',
      'Senior : J\'ai fait plus de 3 projets pro',
      'Senior : J\'ai donné quelques cours',
      'Senior : Je n\'arrive plus à compter le nombre d\'années',
      'Expert : Vous êtes incollable et prêt à relever n\'importe quel défi !'
    ];


  }

  ngOnInit() {

    this.selectedSkills = [];
    this.usersToDisplay = [];
    this.mapSkills = new Map();
    this.mapDif = new Map();
    this.dropdownSettings = {
      singleSelection: false,
      idField: 'item_id',
      textField: 'item_text',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 3,
      allowSearchFilter: true,
      enableCheckAll: false
    };

      const observable = this.skillService.findAll();
      observable.subscribe( data => {
         this.allSkills = data ;
         this.dropdownList = [] ;
         for (const elem of this.allSkills ) {
            let name: string;
            name = String(elem.name);
           this.dropdownList.push({item_id: elem.id, item_text: name});
         }
      });

  }

  onChange() {


  }

  onSelect(item: any) {
    const inductionSkill: InductionSkill = {
      id: item.item_id,
      name: item.item_text,
      level: 10
    };
    this.mapSkills.set(item.item_id, inductionSkill);
    this.skillsSearch();
  }

  onDeSelect(item: any) {
    this.mapSkills.delete(item.item_id);
    this.skillsSearch();
  }

  handleChange(e, id) {
    const inductionSkill: InductionSkill = this.mapSkills.get(id);
    inductionSkill.level = e.value;
    this.mapSkills.set(id, inductionSkill);
    this.orderProfiles();
  }

  ngOnDestroy() {

  }



  skillsSearch() {
    this.userService.searchUsersForRh(Array.from(this.mapSkills.values())).subscribe(data => {
      this.usersSearch = data;
    this.orderProfiles();
    });
  }

  // TO-DO TRI TABLEAU
  orderProfiles() {
    this.mapDif.clear();
    for (const user of this.usersSearch) {
     let tmpScore = 0;
      for (const userskill of user.userSkills) {
        if (this.mapSkills.get(userskill.id.skill.id)) {
          const tmpDif = userskill.score - this.mapSkills.get(userskill.id.skill.id).level ;
          tmpScore += Math.abs(tmpDif);
        }
      }
      //Map pour trier par la suite
      this.mapDif.set(user.id, tmpScore);

    }
    //Tri tab
    this.usersSearch.sort((leftSide, rightSide): number => {
      if (this.mapDif.get(leftSide.id) < this.mapDif.get(rightSide.id)) { return -1 ; }
      if (this.mapDif.get(leftSide.id) > this.mapDif.get(rightSide.id)) { return 1 ; }
      return 0;
    });
  }

  isCompany(userId: number): boolean {
    // Si l'utilisateur est une entreprise
    console.log(userId);
    if (this.comp && this.comp.name && this.usersToDisplay.includes(userId)) {
        return true;
    }
    return false;
  }
  loadUser(userId: number) {
    if (localStorage.getItem('currentUser')) {
      this.comp = JSON.parse(localStorage.getItem('currentUser')).user;
      this.usersToDisplay.push(userId);
    } else {
      //Code a executer si le recruteur n'est pas connecté (pop up de redirection inscription ou login)
      this.display = true;
    }

  }
}
