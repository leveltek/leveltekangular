import { QcmService } from './services/qcm/qcm.service';
import { QcmComponent } from './components/qcm/qcm.component';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { AuthComponent } from './authenticate/auth/auth.component';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { AuthService } from './authenticate/auth.service';
import { AuthGuard } from './authenticate/auth.guard';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { HeaderComponent } from './components/header/header.component';
import { UserService } from './services/user/user.service';
import { UserProfileComponent } from './components/user-profile/user-profile.component';
import { SubscribeComponent } from './components/subscribe/subscribe.component';
import { HomeComponent } from './components/home/home.component';
import { AddSkillsComponent } from './components/add-skills/add-skills.component';

import {ButtonModule} from 'primeng/button';
import {DropdownModule} from 'primeng/dropdown';
import {MultiSelectModule} from 'primeng/multiselect';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import {DialogModule} from 'primeng/dialog';

import { InputTextModule } from 'primeng/inputtext';
import { LevelComponent } from './components/level/level.component';
import { SliderModule } from 'primeng/slider';
import { RhSkillsSearchComponent } from './components/rh-skills-search/rh-skills-search.component';
import { DevSignUpComponent } from './components/dev-sign-up/dev-sign-up.component';
import { DevMainComponent } from './components/dev-main/dev-main.component';
import { UpdateProfileComponent } from './components/update-profile/update-profile.component';
import { AdminComponent } from './components/admin/admin.component';
import { RankComponent } from './components/rank/rank.component';
import { LocationComponent } from './components/location/location.component';
import { JwtInterceptor } from './authenticate/JwtInterceptor';
import { ErrorInterceptor } from './authenticate/ErrorInterceptor';
import { SelectTestComponent } from './components/select-test/select-test.component';
import { TestDevComponent } from './components/test-dev/test-dev.component';
import { TooltipModule } from 'ng2-tooltip-directive';

@NgModule({
  declarations: [
    AppComponent,
    AuthComponent,
    UserProfileComponent,
    SubscribeComponent,
    QcmComponent,
    HeaderComponent,
    HomeComponent,
    AddSkillsComponent,
    RhSkillsSearchComponent,
    DevSignUpComponent,
    LevelComponent,
    UpdateProfileComponent,
    LevelComponent,
    DevMainComponent,
    AdminComponent,
    RankComponent,
    LocationComponent,
    SelectTestComponent,
    TestDevComponent
  ],
  imports: [
  BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    FontAwesomeModule,
    ButtonModule,
    BrowserAnimationsModule,
    InputTextModule,
    SliderModule,
    DropdownModule,
    MultiSelectModule,
    NgMultiSelectDropDownModule,
    DialogModule,
    TooltipModule
  ],
  providers: [AuthService, AuthGuard, UserService, QcmService, { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true},
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true}],
  bootstrap: [AppComponent]
})
export class AppModule { }
