import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject, Observable} from 'rxjs';
import { map } from 'rxjs/operators';
import { User } from '../services/user/user';
import { HttpClient } from '@angular/common/http';
import { StoreService } from '../services/_store/store.service';

@Injectable()
export class AuthService {

  private currentUserSubject: BehaviorSubject<User>;
  public currentUSer: Observable<User>;

  constructor(private myRoute: Router, private http2: HttpClient, private store: StoreService) {
      //AJOUT SPRING
      this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('currentUser')));
      this.currentUSer = this.currentUserSubject.asObservable();
  }

 sendToken(token: string) {
   localStorage.setItem('LoggedInUser', token);
   this.myRoute.navigate(['profile']);
 }
 getToken() {
   //return localStorage.getItem('LoggedInUser');
   return localStorage.getItem('currentUser');
 }
 isLoggedIn() {
   return this.getToken() !== null;
 }
 logout() {
   //localStorage.removeItem('LoggedInUser');
   localStorage.removeItem('currentUser');
   //localStorage.removeItem('LoggedInUser');
   this.myRoute.navigate(['']);
 }


  //Login with spring
  loginSpring(usernameOrEmail: String, password: String) {
    return this.http2.post<any>(`http://localhost:8080/api/auth/signin`, {usernameOrEmail, password}).pipe(map(user => {
       localStorage.setItem('currentUser', JSON.stringify(user));
       this.currentUserSubject.next(user);
       //console.log(user);

       return user;
    }));
  }

  public get currentUserValue(): User {
    return this.currentUserSubject.value;
  }

  logoutSpring() {
    localStorage.removeItem('currentUser');
    this.currentUserSubject.next(null);
  }


}
