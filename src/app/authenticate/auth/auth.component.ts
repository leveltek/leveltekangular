import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormsModule, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { User } from '../../services/user/user';
import { AuthService } from '../auth.service';
import { UserService } from '../../services/user/user.service';
import { BehaviorSubject, Observable } from 'rxjs';
import { StoreService } from '../../services/_store/store.service';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss']
})
export class AuthComponent implements OnInit {
  client: User;
  private clientsUrl: string;
  form;
  submitted = false;
  loading = false;

  constructor(private fb: FormBuilder,
    private myRoute: Router,
    private auth: AuthService, private http: HttpClientModule, private clientService: UserService) {

     this.client = new User();
     this.form = fb.group({
      login: ['', [Validators.required]],
      password: ['', Validators.required]
    });
  }
  ngOnInit() {
  }

  login() {
   this.clientService.checkLogin(this.client).subscribe( res => {
     if (res && res !== 0) {
      localStorage.setItem('UserId', res + '');
      this.auth.sendToken(this.form.value.login);
     }
    }, err => {
      console.log(err);
      this.myRoute.navigate(['home']);
    });
  }

  loginSpring() {
    console.log('TESTATATTATA');
    this.submitted = true;
    /*if (this.form.invalid) {
      console.log('BBBBBBB');
      return;
    }*/
    this.loading = true;
    this.auth.loginSpring(this.client.login, this.client.password).subscribe(data => {
      this.myRoute.navigate(['profil']);
    }, err => {
      console.log(err);
      this.loading = false;
    });
  }

}
