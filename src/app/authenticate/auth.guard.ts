import { Injectable } from '@angular/core';
import { CanActivate, CanActivateChild, CanLoad, Route, UrlSegment, ActivatedRouteSnapshot,
RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable, BehaviorSubject } from 'rxjs';
import { AuthService} from './auth.service';
import { User } from '../services/user/user';

@Injectable()
export class AuthGuard implements CanActivate {

  constructor(private auth: AuthService,
    private myRoute: Router) {
  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean  {
     /* if (this.auth.isLoggedIn()) {
        return true;
      } else {
        this.myRoute.navigate(['home']);
        return false;
      }*/
      const currentUser = this.auth.currentUserValue;
      if (currentUser) {
        return true;
      }
      this.myRoute.navigate(['/login'], {queryParams: {returnUrl: state.url}});
      return false;
    }
  }

