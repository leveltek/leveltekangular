import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { AuthService } from './auth.service';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Injectable } from '@angular/core';

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {

   constructor(private authenticationService: AuthService) { }

   intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
       return next.handle(request).pipe(catchError((err: any) => {
           if (err.status === 401) {
               this.authenticationService.logout();
               location.reload(true);
           }
           console.log(err);
           const error = err.error.message || err.statusText;
           return throwError(error);
       }));
   }
}
