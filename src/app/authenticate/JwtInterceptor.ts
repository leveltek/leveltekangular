import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { AuthService } from './auth.service';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';

@Injectable()
export class JwtInterceptor implements HttpInterceptor {
   constructor(private authenticationService: AuthService) {}
   intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
       let currentUser = this.authenticationService.currentUserValue;
       if (currentUser && currentUser.accessToken) {
           request = request.clone({
               setHeaders: {
                   Authorization: `${currentUser.tokenType} ${currentUser.accessToken}`
               }
           });
       }
       return next.handle(request);
   }

}
