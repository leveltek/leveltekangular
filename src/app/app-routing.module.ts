import { UpdateProfileComponent } from './components/update-profile/update-profile.component';
import { AdminComponent } from './components/admin/admin.component';
import { RhSkillsSearchComponent } from './components/rh-skills-search/rh-skills-search.component';
import { QcmComponent } from './components/qcm/qcm.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { AuthComponent } from './authenticate/auth/auth.component';
import { UserProfileComponent } from './components/user-profile/user-profile.component';
import { AuthGuard } from './authenticate/auth.guard';
import { SubscribeComponent } from './components/subscribe/subscribe.component';
import { HomeComponent } from './components/home/home.component';
import { AddSkillsComponent } from './components/add-skills/add-skills.component';
import { LevelComponent } from './components/level/level.component';
import { DevSignUpComponent } from './components/dev-sign-up/dev-sign-up.component';
import { DevMainComponent } from './components/dev-main/dev-main.component';
import { RankComponent } from './components/rank/rank.component';
import { LocationComponent } from './components/location/location.component';
import { SelectTestComponent } from './components/select-test/select-test.component';
import { TestDevComponent } from './components/test-dev/test-dev.component';

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'home', component: AuthComponent },
  { path: 'profil', component: UpdateProfileComponent, canActivate: [AuthGuard] },
  { path: 'qcm', component: QcmComponent, canActivate: [AuthGuard] },
  { path: 'subscribe', component: SubscribeComponent },
  { path: 'addskills', component: AddSkillsComponent },
  { path: 'level', component: LevelComponent },
  { path: 'devsignup', component: DevSignUpComponent },
  { path: 'rh', component: RhSkillsSearchComponent },
  { path: 'admin', component: AdminComponent },
  { path: 'devmain', component: DevMainComponent },
  { path: 'rank', component: RankComponent },
  { path: 'location', component: LocationComponent},
  { path: 'selectTest', component: SelectTestComponent },
  { path: 'testDev', component: TestDevComponent },
  {path: '**', redirectTo: '/home'}
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule],
  declarations: []
})
export class AppRoutingModule { }
