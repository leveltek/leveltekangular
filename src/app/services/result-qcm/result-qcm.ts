import { ResultQcmKey } from '../result-qcm-key/result-qcm-key';

export class ResultQcm {
  id: ResultQcmKey;
  score: number;
}
