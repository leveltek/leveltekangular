export class Answer {
  id: number;
  libelle: String;
  trueOrFalse: boolean;
  number: number;
}
