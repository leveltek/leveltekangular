import { UserSkillKey } from './../user-skill-key/user-skill-key';
export class UserSkill {
  id: UserSkillKey;
  score: number;

  //AuditModel
  updateAt: Date;
  createdAt: Date;
}
