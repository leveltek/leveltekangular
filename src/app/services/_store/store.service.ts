import { Injectable } from '@angular/core';
import { UserSkill } from '../user-skill/user-skill';

export interface ConnectedDev {
  id: number;
  login: string;
  totalScore: number;
}

@Injectable({
  providedIn: 'root'
})
export class StoreService {

  inductionSkills;
  transitUserSkills: UserSkill[] ;
  selectSkill: string;

  connectedDev: ConnectedDev = null;

  constructor() {

   }

   ngOnInit() {
    this.transitUserSkills = [];
   }
}
