import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Qcm } from './qcm';
import { Observable } from 'rxjs';

// @Injectable({
//   providedIn: 'root'
// })
@Injectable()
export class QcmService {
  private qcmsUrl: string;

  constructor(private http: HttpClient) {
    this.qcmsUrl = 'http://localhost:8080/qcms';
  }

  public findAll(): Observable<Qcm[]> {
    return this.http.get<Qcm[]>(this.qcmsUrl);
  }

  public findById(idQcm: number): Observable<Qcm> {
    return this.http.get<Qcm>(this.qcmsUrl + '/' + idQcm);
  }

  public save(qcm: Qcm) {
    return this.http.post<Qcm>(this.qcmsUrl, qcm);
  }
}
