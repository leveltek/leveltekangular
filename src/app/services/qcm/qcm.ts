import { Question} from '../question-answer/question';

export class Qcm {
  id: number;
  name: String;
  questions: Question[];

}
