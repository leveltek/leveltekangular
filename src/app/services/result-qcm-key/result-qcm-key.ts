import { User } from './../user/user';
import { Qcm } from './../qcm/qcm';

export class ResultQcmKey {
  qcm: Qcm;
  dev: User;
}
