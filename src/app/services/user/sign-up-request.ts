export class SignUpRequest {
  name: String;
  username: String;
  email: String;
  password: String;
  ville: String;
  totalScore: number;
  lookingForWork: boolean;
}
