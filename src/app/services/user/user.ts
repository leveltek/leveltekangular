import { UserSkill } from './../user-skill/user-skill';
import { ResultQcm } from '../result-qcm/result-qcm';
import { Role } from './role';

export class User {
  id: number;
  login: String;
  password: String;
  mail: String;
  adress: String;
  cp: String;
  city: String;
  type: String;

  //Company
  siretNum: String;
  name: String;

  //Dev
  firstName: String;
  lastName: String;
  dateDeNaissance: Date;
  totalScore: number;
  lookingForWork: boolean;
  scores: Map<Number, ResultQcm> ;
  userSkills: UserSkill[];

  //AuditModel
  updateAt: Date;
  createdAt: Date;

  //Authent
  accessToken: String;
  tokenType: String;
  roles: Role[];
}
