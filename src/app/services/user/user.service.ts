import { UserSkill } from './../user-skill/user-skill';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { User } from './user';
import { Observable } from 'rxjs';
import { ResultQcm } from '../result-qcm/result-qcm';
import { Qcm } from '../qcm/qcm';
import { SignUpRequest } from './sign-up-request';
import { InductionSkill } from './../../components/add-skills/add-skills.component';

@Injectable()
export class UserService {

  private clientsUrl: string;
  private clientUrl: string;
  save: any;

  constructor(private http: HttpClient) {
    this.clientsUrl = 'http://localhost:8080/users';
    this.clientUrl = 'http://localhost:8080/login';
  }

  public findAll(): Observable<User[]> {
    return this.http.get<User[]>(this.clientsUrl);
  }

  public devsFindAll(): Observable<User[]> {
    return this.http.get<User[]>('http://localhost:8080/devs');
  }

  public findById(idCli: number): Observable<User> {
    return this.http.get<User>(this.clientsUrl + '/' + idCli);
  }

  public saveCompany(company: User) {
    return this.http.post<User>('http://localhost:8080/companies', company);
  }

  public saveDev(dev: User) {
    return this.http.post<User>('http://localhost:8080/devs', dev);
  }

  public subscribe(signUpRequest: SignUpRequest){
    return this.http.post<User>('http://localhost:8080/api/auth/signup', signUpRequest);
  }

  public checkLogin(cli: User): Observable<Number> {
    return this.http.post<Number>(this.clientUrl, cli);
  }

  public addResultQcm(qcm: Qcm, idCli: number, score: number) {
    return this.http.post<ResultQcm>(this.clientsUrl + '/result_qcm/' + idCli + '/' + score, qcm);
  }

  public addUserSkills(userSkills: UserSkill[]): Observable<UserSkill[]> {
    return this.http.post<UserSkill[]>('http://localhost:8080/user_skills', userSkills);
  }

  public searchUsersForRh(inducSkills: InductionSkill[]): Observable<User[]> {
    return this.http.post<User[]>('http://localhost:8080/rh_search', inducSkills);
  }

  public deleteUserSkills(userSkills: UserSkill[]): Observable<UserSkill[]> {
    return this.http.post<UserSkill[]>('http://localhost:8080/user_skills/delete', userSkills);
  }

  public deleteUserSkill(userSkill: UserSkill): Observable<UserSkill> {
    return this.http.post<UserSkill>('http://localhost:8080/user_skills/delete_one', userSkill);
  }
}
