import { UserSkill } from './../user-skill/user-skill';
export class Skill {
  id: number ;
  userSkills: UserSkill[];
  name: String;
}
