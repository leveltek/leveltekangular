import { Skill } from './skill';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { User } from '../user/user';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class SkillService {

  private skillsUrl: string;

  constructor(private http: HttpClient) {
    this.skillsUrl = 'http://localhost:8080/skills';
  }

  public findAll(): Observable<Skill[]> {
    return this.http.get<User[]>(this.skillsUrl);
  }
}
