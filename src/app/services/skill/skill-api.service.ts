import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

export interface ISkill {
  id: number;
  name: string;
}

export interface ISkillApiService {
  getSkills();
  getSkillsByText(name: string);
  addSkill(name: string);
}

@Injectable({
  providedIn: 'root'
})
export class SkillApiService implements ISkillApiService  {

  url = 'http://localhost:8080/skills/';

  constructor(private http: HttpClient) { }

  getSkills() {
    return this.http.get(this.url);
  }

  getSkillsByText(name: string) {
    return this.http.get(`${this.url}?search=${name}`);
  }
  addSkill(name: string) {
    const body = {
      name
    };
    return this.http.post(this.url, body);
  }
}
